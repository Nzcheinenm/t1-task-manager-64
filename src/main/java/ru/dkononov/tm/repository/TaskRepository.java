package ru.dkononov.tm.repository;

import org.springframework.stereotype.Repository;
import ru.dkononov.tm.model.Task;

import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("DEMO"));
    }

    public void create() {
        add(new Task("New Task " + LocalDate.now().toString()));
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(String id) {
        return tasks.get(id);
    }

    public void removeById(String id) {
        tasks.remove(id);
    }

}
