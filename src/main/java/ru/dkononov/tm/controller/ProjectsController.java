package ru.dkononov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.dkononov.tm.repository.ProjectRepository;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list","projects",projectRepository.findAll());
    }

}
