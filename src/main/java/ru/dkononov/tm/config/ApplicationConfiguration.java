package ru.dkononov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.dkononov.tm")
public class ApplicationConfiguration {
}
