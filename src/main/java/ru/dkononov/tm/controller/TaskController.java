package ru.dkononov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.dkononov.tm.enumerated.Status;
import ru.dkononov.tm.model.Project;
import ru.dkononov.tm.model.Task;
import ru.dkononov.tm.repository.ProjectRepository;
import ru.dkononov.tm.repository.TaskRepository;

import java.time.LocalDate;
import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new Task("New Task " + LocalDate.now().toString()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskRepository.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task",task);
        modelAndView.addObject("projects",getProjects());
        modelAndView.addObject("statuses",getStatuses());
        return modelAndView;
    }

    private Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    private Status[] getStatuses() {
        return Status.values();
    }

}
