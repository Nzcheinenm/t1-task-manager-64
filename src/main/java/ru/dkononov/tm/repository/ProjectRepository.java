package ru.dkononov.tm.repository;

import org.springframework.stereotype.Repository;
import ru.dkononov.tm.model.Project;

import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("DEMO"));
    }

    public void create() {
        add(new Project("New Project " + LocalDate.now().toString()));
    }

    public void add(Project project) {
        projects.put(project.getId(), project);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(String id) {
        return projects.get(id);
    }

    public void removeById(String id) {
        projects.remove(id);
    }

}
